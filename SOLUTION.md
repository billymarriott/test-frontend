# SOLUTION

## Estimation

Estimated: **6 hours**
Spent: **8 hours**

The solution did take slightly longer than I first anticipated. I put this primarily down to some of the edge cases that I encountered, especially with the price filter.

See below a link to the trello board I used to plan the solution and keep track of my progress.

[Trello Board link](https://trello.com/invite/b/zzuyG9tX/a422dd1c41a1202d91df0044bdced28e/petlab-technical-test)

## Solution

For this challenge, I've stuck to the brief and have tried my best to cover all the edge cases with the time that I had. The solution passes all of the criteria with some small additions such as a price range and a no results fallback. I've kept the design simple but have made it work responsively. Unfortunately, I haven't had time to test everything. However, I have added unit tests for the `mapFiltersToParams` utility as well as the `SubscriptionFilter` component.

The most important part of this solution is the filter logic. I have built these in a way that's relatively agnostic and easily extendable. The filters are stored as objects and then mapped to query parameters. The query parameters are stored at the top level and when updated they trigger `useProducts` hook. This hook then makes a request with the updated query parameters and the updated products are then passed back to the product grid. It's also worth mentioning that `debounce` is used on the pricing and search filters to delay the update as the update is triggered via an `onChange` event.

Thank you very much for taking the time to look through my solution! 😀
