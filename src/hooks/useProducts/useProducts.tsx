import axios from 'axios';
import { useEffect, useState } from 'react';
import { MAX_PRODUCTS } from './useProducts.constants';
import { Product } from '../../types/product.types';

const useProducts = (queryParams: string, pageNumber: number = 1) => {
  const [products, setProducts] = useState<Product[]>([]);
  const [pageCount, setPageCount] = useState<number>(1);

  useEffect(() => {
    const pageQueryPrefix = !!queryParams ? '&' : '';
    const queryParamsWithPageNumber = `?${queryParams}${pageQueryPrefix}_page=${pageNumber}&_limit=${MAX_PRODUCTS}`;

    // Get products
    axios
      .get(`http://localhost:3010/products${queryParamsWithPageNumber}`)
      .then((response) => {
        setProducts(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });

    // Gets product count and returns total pages.
    // Needs an extra request as pageNumber needs to be omitted to get the full count.
    axios
      .get(`http://localhost:3010/products?${queryParams}`)
      .then((response) => {
        setPageCount(Math.ceil(response.data.length / MAX_PRODUCTS));
      })
      .catch(function (error) {
        console.log(error);
      });
  }, [queryParams, pageNumber]);

  return { products, pageCount };
};

export default useProducts;
