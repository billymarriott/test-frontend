export interface FilterShared {
  filterKey: string;
  type: 'range' | 'search' | 'value';
}

export interface RangeFilter extends FilterShared {
  min: number;
  max: number;
  value?: never;
}

export interface SearchFilter extends FilterShared {
  value: string;
  min?: never;
  max?: never;
}
export interface ValueFilter extends FilterShared {
  value: string;
  min?: never;
  max?: never;
}

export type Filter = RangeFilter | SearchFilter | ValueFilter;
