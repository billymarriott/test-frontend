import React from 'react';
import ReactDOM from 'react-dom';
import ResetStyles from './styles/reset.styles';
import GlobalStyles from './styles/global.styles';
import App from './pages/App';

ReactDOM.render(
  <React.StrictMode>
    <ResetStyles />
    <GlobalStyles />
    <App />
  </React.StrictMode>,
  document.getElementById('root'),
);
