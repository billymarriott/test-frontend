import styled from 'styled-components';

export const ProductContainer = styled.article`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  background-color: #fff;
  border-radius: 5px;
  padding: 25px;
`;

export const ProductImage = styled.img`
  max-width: 100%;
  max-height: 200px;
`;

export const ProductTitle = styled.h2`
  font-size: 20px;
  margin: 15px 0;
  text-align: center;
`;

export const ProductPrice = styled.span`
  margin-top: 15px;
  color: #c95f3d;
  font-weight: 300;
  font-size: 18px;
`;

export const ProductButton = styled.a`
  text-decoration: none;
  margin-top: auto;
  background-color: #e26447;
  color: #fff;
  border: none;
  font-weight: 700;
  padding: 10px 0;
  border-radius: 5px;
  font-size: 20px;
  cursor: pointer;
  font-weight: 400;
  width: 100%;
  max-width: 250px;
  text-align: center;

  &:hover {
    background-color: #c95f3d;
  }
`;
