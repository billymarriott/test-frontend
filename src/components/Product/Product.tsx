import { Product as ProductInterface } from '../../types/product.types';
import * as Styles from './Product.styles';

interface ProductProps extends ProductInterface {}

const Product: React.FC<ProductProps> = ({ title, image_src, price, url }) => {
  return (
    <Styles.ProductContainer>
      <Styles.ProductImage src={image_src} alt="Product image" />
      <Styles.ProductPrice>£{price}</Styles.ProductPrice>
      <Styles.ProductTitle>{title}</Styles.ProductTitle>
      <Styles.ProductButton tabIndex={0} href={url} target="_blank">
        Shop now
      </Styles.ProductButton>
    </Styles.ProductContainer>
  );
};

export default Product;
