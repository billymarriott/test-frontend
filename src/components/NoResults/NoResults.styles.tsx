import styled from 'styled-components';
import breakpoints from '../../styles/breakpoints.styles';

export const NoResultsContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background: #fff;
  border-radius: 5px;
  width: 100%;
  min-height: 200px;

  @media ${breakpoints.medium} {
    width: 65%;
  }

  @media ${breakpoints.large} {
    width: 75%;
  }
`;
