import * as Styles from './NoResults.styles';

const NoResults = () => {
  return <Styles.NoResultsContainer>No results found</Styles.NoResultsContainer>;
};

export default NoResults;
