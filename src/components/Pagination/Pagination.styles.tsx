import styled from 'styled-components';

export const PaginationContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: flex-end;
  margin-top: 50px;
`;

export const PaginationButton = styled.button`
  color: #c95f3d;
  border: 1px solid #c95f3d;
  font-size: 18px;
  background-color: unset;
  border-radius: 5px;
  padding: 10px;
  cursor: pointer;

  &:last-child {
    margin-left: 10px;
  }

  &:disabled {
    color: #666;
    border: 1px solid #666;
    cursor: not-allowed;
  }

  &:hover:not(:disabled) {
    background-color: #c95f3d;
    color: #fff;
  }
`;
