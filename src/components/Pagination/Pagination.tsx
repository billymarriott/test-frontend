import * as Styles from './Pagination.styles';

interface PaginationProps {
  pageNumber: number;
  setPageNumber: React.Dispatch<React.SetStateAction<number>>;
  totalPageCount: number;
}

const Pagination: React.FC<PaginationProps> = ({ pageNumber, setPageNumber, totalPageCount }) => {
  const minPageNumber = 1;
  const isMinPage = pageNumber === minPageNumber;
  const isMaxPage = pageNumber === totalPageCount;

  return (
    <Styles.PaginationContainer>
      <Styles.PaginationButton
        onClick={() => setPageNumber(pageNumber - 1)}
        title="Previous page"
        aria-label="Navigate to previous page"
        disabled={isMinPage}
      >
        {'<'}
      </Styles.PaginationButton>
      <Styles.PaginationButton
        onClick={() => setPageNumber(pageNumber + 1)}
        title="Next page"
        aria-label="Navigate to next page"
        disabled={isMaxPage}
      >
        {'>'}
      </Styles.PaginationButton>
    </Styles.PaginationContainer>
  );
};

export default Pagination;
