import { useState } from 'react';
import { ValueFilter } from '../../types/filter.types';
import * as constants from './SubscriptionFilter.constants';
import * as Styles from './SubscriptionFilter.styles';

interface SubscriptionFilterProps {
  updateFilters: (filterData: ValueFilter) => void;
}

const SubscriptionFilter: React.FC<SubscriptionFilterProps> = ({ updateFilters }) => {
  const [subscriptionSelection, setSubscriptionSelection] = useState<string>();

  const updateSubscriptionFilter = (subscriptionType: string) => {
    let selection = '';

    // Uses strings so we can pass back an empty string and remove the filter if a user toggles the value.
    if (subscriptionType === 'true') {
      selection = subscriptionSelection === 'true' ? '' : 'true';
    }

    if (subscriptionType === 'false') {
      selection = subscriptionSelection === 'false' ? '' : 'false';
    }

    setSubscriptionSelection(selection);
    updateFilters({
      filterKey: 'subscription',
      type: 'value',
      value: selection,
    });
  };

  const hasTrueSelected = subscriptionSelection === 'true';
  const hasFalseSelected = subscriptionSelection === 'false';

  return (
    <Styles.SubscriptionFilterContainer>
      <Styles.Label>Subscription</Styles.Label>
      <Styles.ButtonWrapper>
        <Styles.SubscriptionButton
          onClick={() => updateSubscriptionFilter('true')}
          isActive={hasTrueSelected}
          aria-label={`${hasTrueSelected ? 'Unselect' : 'Select'} ${constants.SUBSCRIPTION_YES}`}
        >
          {constants.SUBSCRIPTION_YES}
        </Styles.SubscriptionButton>
        <Styles.SubscriptionButton
          onClick={() => updateSubscriptionFilter('false')}
          isActive={hasFalseSelected}
          aria-label={`${hasFalseSelected ? 'Unselect' : 'Select'} ${constants.SUBSCRIPTION_NO}`}
        >
          {constants.SUBSCRIPTION_NO}
        </Styles.SubscriptionButton>
      </Styles.ButtonWrapper>
    </Styles.SubscriptionFilterContainer>
  );
};

export default SubscriptionFilter;
