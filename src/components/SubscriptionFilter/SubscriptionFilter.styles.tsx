import styled, { css } from 'styled-components';

export const SubscriptionFilterContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Label = styled.span`
  display: flex;
  font-size: 18px;
  margin-bottom: 10px;
  width: 100%;
  color: #213f4e;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
`;

interface SubscriptionButtonProps {
  isActive: boolean;
}

export const SubscriptionButton = styled.button<SubscriptionButtonProps>`
  width: 45%;
  color: #c95f3d;
  border: 1px solid #c95f3d;
  font-size: 18px;
  background-color: unset;
  border-radius: 5px;
  padding: 10px 0;
  cursor: pointer;

  ${(props) =>
    props.isActive &&
    css`
      background-color: #c95f3d;
      color: #fff;
    `}
`;
