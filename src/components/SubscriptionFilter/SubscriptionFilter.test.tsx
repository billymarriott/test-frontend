import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import SubscriptionFilter from './SubscriptionFilter';
import * as constants from './SubscriptionFilter.constants';

describe('SubscriptionFilter', () => {
  test('sends back the expected values', async () => {
    const mockFunction = jest.fn();
    const sharedReturnValues = { filterKey: 'subscription', type: 'value' };

    render(<SubscriptionFilter updateFilters={mockFunction} />);

    expect(screen.getByText(constants.SUBSCRIPTION_YES)).toBeInTheDocument();
    expect(screen.getByText(constants.SUBSCRIPTION_NO)).toBeInTheDocument();

    fireEvent.click(screen.getByText(constants.SUBSCRIPTION_YES));
    expect(mockFunction).toBeCalledWith({
      ...sharedReturnValues,
      value: 'true',
    });

    // Second click of the same button removes the value
    fireEvent.click(screen.getByText(constants.SUBSCRIPTION_YES));
    expect(mockFunction).toBeCalledWith({
      ...sharedReturnValues,
      value: '',
    });

    fireEvent.click(screen.getByText(constants.SUBSCRIPTION_NO));
    expect(mockFunction).toBeCalledWith({
      ...sharedReturnValues,
      value: 'false',
    });
  });
});
