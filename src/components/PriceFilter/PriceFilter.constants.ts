export const MIN_ERROR_MSG = 'Please specify a min price lower than the max price';
export const MAX_ERROR_MSG = 'Please specify a max price higher than the min price';
