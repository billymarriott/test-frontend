import styled from 'styled-components';

export const PriceFilterContainer = styled.div`
  display: flex;
  width: 100%;
  margin-bottom: 15px;

  fieldset {
    width: 100%;
  }
`;

export const InputWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const PriceInput = styled.input`
  font-size: 18px;
  width: 45%;
  border: 1px solid #213f4e;
  border-radius: 5px;
  padding: 10px;
`;

export const PriceFilterTitle = styled.legend`
  font-size: 18px;
  margin-bottom: 10px;
  width: 100%;
  color: #213f4e;
`;

export const ErrorMessage = styled.span`
  display: flex;
  font-size: 14px;
  color: red;
  margin-bottom: 10px;
`;
