import { useState } from 'react';
import debounce from 'debounce';
import { RangeFilter } from '../../types/filter.types';
import * as constants from './PriceFilter.constants';
import * as Styles from './PriceFilter.styles';

interface PriceFilterProps {
  updateFilters: (filterData: RangeFilter) => void;
}

const PriceFilter: React.FC<PriceFilterProps> = ({ updateFilters }) => {
  const MIN_PRICE = 0;
  // TODO: Set using highest value from products
  const MAX_PRICE = 150;
  const [minPrice, setMinPrice] = useState<number>(MIN_PRICE);
  const [maxPrice, setMaxPrice] = useState<number>(MAX_PRICE);
  const [errorMessage, setErrorMessage] = useState<string>('');

  const clearErrorMessage = () => {
    if (errorMessage) setErrorMessage('');
  };

  const updateValues = debounce((event: React.ChangeEvent<HTMLInputElement>, rangeType: 'min' | 'max') => {
    clearErrorMessage();

    const isMinUpdate = rangeType === 'min';
    const isMaxUpdate = rangeType === 'max';
    const fallbackValue = isMinUpdate ? MIN_PRICE : MAX_PRICE;
    const value = event.target.value.length ? Number(event.target.value) : fallbackValue;

    if (isMinUpdate && value > maxPrice) setErrorMessage(constants.MIN_ERROR_MSG);
    if (isMaxUpdate && value < minPrice) setErrorMessage(constants.MAX_ERROR_MSG);

    updateFilters({
      filterKey: 'price',
      type: 'range',
      min: isMinUpdate ? value : minPrice,
      max: isMaxUpdate ? value : maxPrice,
    });

    if (isMinUpdate) setMinPrice(value);
    if (isMaxUpdate) setMaxPrice(value);
  }, 500);

  return (
    <Styles.PriceFilterContainer>
      <fieldset>
        <Styles.PriceFilterTitle>Price</Styles.PriceFilterTitle>
        {!!errorMessage && <Styles.ErrorMessage>{errorMessage}</Styles.ErrorMessage>}
        <Styles.InputWrapper>
          <Styles.PriceInput
            type="number"
            placeholder="Min"
            onChange={(e) => updateValues(e, 'min')}
            aria-label="Minimum price"
            min="0"
            max={maxPrice}
          />
          <Styles.PriceInput
            type="number"
            placeholder="Max"
            onChange={(e) => updateValues(e, 'max')}
            aria-label="Maximum price"
            min={minPrice}
          />
        </Styles.InputWrapper>
      </fieldset>
    </Styles.PriceFilterContainer>
  );
};

export default PriceFilter;
