import { Product as ProductInterface } from '../../types/product.types';
import Product from '../Product';
import NoResults from '../NoResults';
import * as Styles from './ProductGrid.styles';

interface ProductGridProps {
  products: ProductInterface[];
}

const ProductGrid: React.FC<ProductGridProps> = ({ products }) => {
  const hasProducts = !!products.length;

  if (!hasProducts) return <NoResults />;

  return (
    <Styles.ProductGridContainer>
      {products.map((product) => {
        return <Product {...product} key={product.id} />;
      })}
    </Styles.ProductGridContainer>
  );
};

export default ProductGrid;
