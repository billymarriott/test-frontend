import styled from 'styled-components';
import breakpoints from '../../styles/breakpoints.styles';

export const ProductGridContainer = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  gap: 15px;

  @media ${breakpoints.small} {
    grid-template-columns: repeat(2, 1fr);
  }

  @media ${breakpoints.medium} {
    width: 65%;
  }

  @media ${breakpoints.large} {
    width: 75%;
    grid-template-columns: repeat(3, 1fr);
  }
`;
