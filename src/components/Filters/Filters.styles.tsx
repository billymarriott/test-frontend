import styled from 'styled-components';
import breakpoints from '../../styles/breakpoints.styles';

export const FilterContainer = styled.aside`
  width: 100%;
  margin-bottom: 25px;

  @media ${breakpoints.medium} {
    margin-bottom: unset;
    padding-right: 25px;
    width: 35%;
  }

  @media ${breakpoints.large} {
    width: 25%;
  }
`;

export const FilterWrapper = styled.div`
  background: #fff;
  width: 100%;
  border-radius: 5px;
  padding: 25px;
`;

export const FilterHeader = styled.h2`
  font-size: 22px;
  width: 100%;
  padding-bottom: 10px;
  margin-bottom: 15px;
  color: #213f4e;
`;
