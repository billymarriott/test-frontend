import { useState } from 'react';
import { Filter } from '../../types/filter.types';
import PriceFilter from '../PriceFilter';
import SearchFilter from '../SearchFilter';
import mapFiltersToParams from '../../utils/mapFiltersToParams';
import * as Styles from './Filters.styles';
import SubscriptionFilter from '../SubscriptionFilter';

interface FiltersProps {
  setQueryParams: React.Dispatch<React.SetStateAction<string>>;
}

const Filters: React.FC<FiltersProps> = ({ setQueryParams }) => {
  const [filters, setFilters] = useState<Filter[]>([]);

  const updateFilters = (updatedFilter: Filter) => {
    const updatedFilters = [...filters];
    const filterIndex = filters.findIndex((filter) => filter.filterKey === updatedFilter.filterKey);

    if (filterIndex === -1) {
      updatedFilters.push(updatedFilter);
    } else {
      updatedFilters[filterIndex] = updatedFilter;
    }

    setFilters(updatedFilters);
    setQueryParams(mapFiltersToParams(updatedFilters));
  };

  return (
    <Styles.FilterContainer>
      <Styles.FilterWrapper>
        <Styles.FilterHeader>Filters</Styles.FilterHeader>
        <SearchFilter updateFilters={updateFilters} />
        <PriceFilter updateFilters={updateFilters} />
        <SubscriptionFilter updateFilters={updateFilters} />
      </Styles.FilterWrapper>
    </Styles.FilterContainer>
  );
};

export default Filters;
