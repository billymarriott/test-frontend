import styled from 'styled-components';

export const SearchFilterContainer = styled.div`
  display: flex;
  margin-bottom: 15px;
`;

export const SearchInput = styled.input`
  font-size: 18px;
  width: 100%;
  border: 1px solid #213f4e;
  border-radius: 5px;
  padding: 10px;
`;
