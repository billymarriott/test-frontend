import debounce from 'debounce';
import { SearchFilter as ISearchFilter } from '../../types/filter.types';
import * as Styles from './SearchFilter.styles';

interface SearchFilterProps {
  updateFilters: (filterData: ISearchFilter) => void;
}

const SearchFilter: React.FC<SearchFilterProps> = ({ updateFilters }) => {
  const updateSearch = debounce((event: React.ChangeEvent<HTMLInputElement>) => {
    updateFilters({
      filterKey: 'tags',
      type: 'search',
      value: event.target.value,
    });
  }, 500);

  return (
    <Styles.SearchFilterContainer>
      <Styles.SearchInput
        aria-label="Search products"
        type="text"
        placeholder="Search"
        onChange={(e) => updateSearch(e)}
      />
    </Styles.SearchFilterContainer>
  );
};

export default SearchFilter;
