import { css, createGlobalStyle } from 'styled-components';

export const global = css`
  body {
    @import url('https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;700&display=swap');
    font-family: 'Rubik', sans-serif;
    background-color: #e7ebed;
  }
`;

const GlobalStyles = createGlobalStyle`${global}`;

export default GlobalStyles;
