import { Filter, RangeFilter, SearchFilter, ValueFilter } from '../../types/filter.types';

const getRangeParam = (filterData: RangeFilter) => {
  return `${filterData.filterKey}_gte=${filterData.min}&${filterData.filterKey}_lte=${filterData.max}`;
};

const getSearchParam = (filterData: SearchFilter) => {
  return `${filterData.filterKey}_like=${filterData.value}`;
};

const getValueParam = (filterData: ValueFilter) => {
  // If no value is provided, don't add the filter
  if (!filterData.value) return '';

  return `${filterData.filterKey}=${filterData.value}`;
};

const mapFiltersToParams = (filters: Filter[]) => {
  return filters
    .map((filter) => {
      let param = '';
      switch (filter.type) {
        case 'range':
          param = getRangeParam(filter as RangeFilter);
          break;
        case 'search':
          param = getSearchParam(filter as SearchFilter);
          break;
        case 'value':
          param = getValueParam(filter as ValueFilter);
          break;
      }

      return param;
    })
    .join('&');
};

export default mapFiltersToParams;
