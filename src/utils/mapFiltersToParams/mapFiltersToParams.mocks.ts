import { FilterShared } from '../../types/filter.types';

export const MOCK_RANGE_FILTER = {
  filterKey: 'price',
  type: 'range' as FilterShared['type'],
  min: 5,
  max: 10,
};

export const MOCK_SEARCH_FILTER = {
  filterKey: 'tags',
  type: 'search' as FilterShared['type'],
  value: 'dog',
};

export const MOCK_VALUE_FILTER = {
  filterKey: 'subscription',
  type: 'value' as FilterShared['type'],
  value: 'dog',
};
