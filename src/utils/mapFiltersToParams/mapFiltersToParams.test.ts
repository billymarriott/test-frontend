import mapFiltersToParams from '.';
import { MOCK_RANGE_FILTER, MOCK_SEARCH_FILTER, MOCK_VALUE_FILTER } from './mapFiltersToParams.mocks';

describe('mapFiltersToParams', () => {
  it('returns correct parameters for range', async () => {
    expect(mapFiltersToParams([MOCK_RANGE_FILTER])).toBe(
      `${MOCK_RANGE_FILTER.filterKey}_gte=${MOCK_RANGE_FILTER.min}&${MOCK_RANGE_FILTER.filterKey}_lte=${MOCK_RANGE_FILTER.max}`,
    );
  });

  it('returns correct parameters for search', async () => {
    expect(mapFiltersToParams([MOCK_SEARCH_FILTER])).toBe(
      `${MOCK_SEARCH_FILTER.filterKey}_like=${MOCK_SEARCH_FILTER.value}`,
    );
  });

  it('returns correct parameters for value', async () => {
    expect(mapFiltersToParams([MOCK_VALUE_FILTER])).toBe(`${MOCK_VALUE_FILTER.filterKey}=${MOCK_VALUE_FILTER.value}`);
  });
});
