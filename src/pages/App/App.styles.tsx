import styled from 'styled-components';
import breakpoints from '../../styles/breakpoints.styles';

export const AppContainer = styled.main`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  max-width: 1200px;
  margin: 0 auto;
  padding: 50px 15px;

  @media ${breakpoints.xlarge} {
    padding: 50px 5px;
  }
`;
