import { useState } from 'react';
// import logo from './logo.svg';
import ProductGrid from '../../components/ProductGrid';
import Filters from '../../components/Filters';
import useProducts from '../../hooks/useProducts';
import Pagination from '../../components/Pagination';
import * as Styles from './App.styles';

const App = () => {
  const [queryParams, setQueryParams] = useState('');
  const [pageNumber, setPageNumber] = useState(1);
  const { products, pageCount } = useProducts(queryParams, pageNumber);

  return (
    <Styles.AppContainer>
      <Filters setQueryParams={setQueryParams} />
      <ProductGrid products={products} />
      <Pagination pageNumber={pageNumber} setPageNumber={setPageNumber} totalPageCount={pageCount} />
    </Styles.AppContainer>
  );
};

export default App;
